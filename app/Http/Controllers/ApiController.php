<?php

namespace App\Http\Controllers;


use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;

class ApiController extends Controller
{
    /**
     *
     */
    public function index()
    {

//
//        $bbb                 = new BigBlueButton();
//        $createMeetingParams = new CreateMeetingParameters('bbb-meeting-uid-65', 'BigBlueButton API Meeting');
//        $response            = $bbb->createMeeting($createMeetingParams);
//
//        echo "Created Meeting with ID: " . $response->getMeetingId();

        $bbb = new BigBlueButton();


        $meetingID = 'Test Example';
        $meetingName = 'Test Meeting';
        $attendee_password = '1';
        $moderator_password = '1';
        $duration = '200';
        $urlLogout = '1';
        $isRecordingTrue = '1';
        $createMeetingParams = new CreateMeetingParameters($meetingID, $meetingName);
        $createMeetingParams->setAttendeePassword($attendee_password);
        $createMeetingParams->setModeratorPassword($moderator_password);
        $createMeetingParams->setDuration($duration);
        $createMeetingParams->setLogoutUrl($urlLogout);
        if ($isRecordingTrue) {
            $createMeetingParams->setRecord(true);
            $createMeetingParams->setAllowStartStopRecording(true);
            $createMeetingParams->setAutoStartRecording(true);
        }

        $response = $bbb->createMeeting($createMeetingParams);
        if ($response->getReturnCode() == 'FAILED') {
            return 'Can\'t create room! please contact our administrator.';
        } else {
            // process after room created
//            dump("created");
        }


        $bbb = new BigBlueButton();
        $name = request('name');
        $password = '1';
        $joinMeetingParams = new JoinMeetingParameters($meetingID, $name, $password); // $moderator_password for moderator
        $joinMeetingParams->setRedirect(true);
        $url = $bbb->getJoinMeetingURL($joinMeetingParams);

        request()->session()->push('teacher.url', $url);


        return redirect($url);
    }

    public function studentUrl()
    {
        return redirect(session('teacher.url.0'));
    }
}
